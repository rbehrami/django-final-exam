from django import forms
from django.forms.models import inlineformset_factory
from .models import SignUp, Course, Step



class CourseForm(forms.ModelForm):
	class Meta:
		model = Course
		# fields = '__all__'
		fields = ['title', 'description']	

# class StepForm(forms.ModelForm):
# 	class Meta:
# 		model = Step
# 		fields = '__all__'

# StepFormSet = inlineformset_factory(Course, Step, fields='__all__', extra=1)


class SignUpForm(forms.ModelForm):
	class Meta:
		model = SignUp
		fields = ['full_name','email']		

	def clean_email(self):
		email = self.cleaned_data.get('email')
		email_base, provider = email.split("@")
		domain, extension = provider.split('.')
		#if not domain == 'USC':		
		#	raise forms.ValidationError('Please make sure you use your USC email.')
		if not extension == "com":
			raise forms.ValidationError("Please use a valid .com, .org, .net email address")
		return email

	def clean_full_name(self):
		full_name = self.cleaned_data.get('full_name')
		return full_name