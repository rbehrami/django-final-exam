from django.test import TestCase
from .models import Course

class CourseModelTests(TestCase):
	def test_course_creation(self):
		course = Course.objects.create(
			title="Python Regular Expressions",
			description="Learn to write regular expresions in Python"
		)

