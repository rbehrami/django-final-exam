from django.contrib.sites.models import Site
from django.db import models

# Create your models here.
class SignUp(models.Model):
	email         = models.EmailField(max_length=100)
	full_name     = models.CharField(max_length=200, blank= True)
	created_time  = models.DateTimeField(auto_now_add = True)
	last_modified = models.DateTimeField(auto_now = True)

	def __unicode__(self):
		return self.email
		#dict = model_to_dict(self)
		#return "\'{full_name}\'<{email}>".format(**dict)

class Course(models.Model):
	created_at   = models.DateTimeField(auto_now_add = True)
	title 		 = models.CharField(max_length=250)
	description  = models.TextField()

	def __unicode__(self): # def __str__(self) it is for python 3.0
		return self.title

class Step(models.Model):
	title 		= models.CharField(max_length=200)
	description = models.TextField()
	content 	= models.TextField(blank=True, default='')
	order		= models.IntegerField(default=0)
	course 		= models.ForeignKey(Course)

	class Meta:
		ordering = ['order',]

	def __unicode__(self):
		return self.title


