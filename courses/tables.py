# tutorial/tables.py
import django_tables2 as tables
from courses.models import Course

class CourseTable(tables.Table):
    class Meta:
        model = Course
        # add class="paleblue" to <table> tag
        attrs = {"class": "paleblue"}