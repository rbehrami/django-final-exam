# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0002_auto_20151030_1120'),
    ]

    operations = [
        migrations.AlterField(
            model_name='step',
            name='course',
            field=models.ForeignKey(to='courses.Course'),
        ),
    ]
