from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.course_list, name='list'),
	url(r'(?P<course_pk>\d+)/(?P<step_pk>\d+)/$', views.step_detail, name='step'),
	url(r'(?P<pk>\d+)/$', views.course_detail, name='detail'),
	url(r'^course/new/$', views.new_course, name='new_course'),
	url(r'^course/(?P<pk>[0-9]+)/edit/$', views.course_edit, name='course_edit'),
	# url(r'^course/edit/$', views.edit_course, name='edit_course'),
]