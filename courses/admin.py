from django.contrib import admin

from . import models
from .forms import SignUpForm
from .models import Course, Step, SignUp


class StepInline(admin.TabularInline):
	model = models.Step
	extra = 1


class CourseAdmin(admin.ModelAdmin):
	inlines = [
	StepInline,
	]

class StepInline(admin.StackedInline):
	model = Step

# class CourseAdmin(admin.ModelAdmin):
# 	inlines = [StepInline,]

class SignUpAdmin(admin.ModelAdmin):
	list_display = ["__unicode__", 'created_time', 'last_modified']
	form = SignUpForm
	#class Meta:
	#	model = SignUp

#@admin.register(SignUp)
#class SignUpAdmin(admin.ModelAdmin):
#	data_heirarchy = 'created_time'

# Register your models here.
admin.site.register(Course, CourseAdmin)
admin.site.register(Step)
admin.site.register(SignUp, SignUpAdmin)