from django.shortcuts import get_object_or_404, render
from django.shortcuts import redirect
from django_tables2 import RequestConfig
from courses.tables import CourseTable
from .forms import CourseForm
from .models import Course, Step

# Create your views here.

# def cslist(request):
# 	table = CourseTable(Course.objects.all())
# 	RequestConfig(request).configure(table)
# 	return render(request, 'cslist/cslist.html', {'table': table})

def course_list(request):
	courses = Course.objects.all()
	return render(request, 'courses/course_list.html', {'courses': courses})

# def cslist(request):
# 	return render (request, "cslist.html", {"table": Course.ojbects.all()})

def course_detail(request, pk):
	course = get_object_or_404(Course, pk=pk)
	return render(request, 'courses/course_detail.html', {'course': course})

def step_detail(request, course_pk, step_pk):
	step = get_object_or_404(Step, course_id=course_pk, pk=step_pk)
	return render(request, 'courses/step_detail.html', {'step': step})


# ============== add new course =========================
def new_course(request):
	#a http POST?
	if request.method == 'POST':
		form = CourseForm(request.POST)

		if form.is_valid():
			form.save(commit=True)
			# return redirect('courses.views.course_detail', pk=post.pk)
			return redirect('courses.views.mycourses')
		else:
			print form.errors
	else:
		form = CourseForm()	
	return render(request, 'courses/new_course.html', {'form': form})


# ============= update course ============================
def course_edit(request, pk):
    course = get_object_or_404(Course, pk=pk)
    if request.method == "POST":
        form = CourseForm(request.POST, instance=course)
        if form.is_valid():
            form.save(commit=True)
            return redirect('courses.views.mycourses')
    else:
        form = CourseForm(instance=course)
    return render(request, 'courses/course_edit.html', {'form': form})


# ============= navigate about page ======================
def about(request):
	return render(request, 'about.html')

# ============= navigate contact page=====================
def contact(request):
	return render(request, 'contact_us.html')


# ============= register user from default 
# ============= we are not using this view ==============
def signup(request):
	title = 'Welcome'
	if request.user.is_authenticated():
		title = "Register"
		form = SignUpForm(request.POST or None)
		context = {
			"title": title,
			"form" : form
		}
		if form.is_valid():
			#form.save()
			#print request.POST['email']
			instance = form.save(commit=False)
			full_name = form.cleaned_data.get("full_name")
			if not full_name:
				full_name = "Unknown User"
			instance.full_name = full_name
			#if not instance.full_name:
			#	instance.full_name = "Unknown User"
			instance.save()
			context = {
				"title": "Thanks for registration"
			
		}
	return render(request, "signup.html", context)

# ============= navigate login page ======================
def login(request):
	return render(request, 'login.html')

# ============= navigate userdashboard page ==============
def userdashboard(request):
	 return render(request, 'userdashboard.html')

# ============= navigate mycourses page ==================
def mycourses(request):
	courses = Course.objects.all()
	return render(request, 'mycourses.html', {'courses': courses})

# ============= navigate users page ======================
def users(request):
	return render(request, 'users.html')