"""learningsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
# from api import CourseResource
from django.contrib import admin
from views import cslist


from . import views

urlpatterns = [
    url(r'^$', views.home),
	url(r'^courses/', include('courses.urls', namespace='courses')),
    url(r'^cslist/', cslist, name='cslist'),
    url(r'^about/$', 'courses.views.about', name='about'),
    url(r'^contact/$', 'courses.views.contact', name='contact'),
    url(r'^login/$', 'courses.views.login', name='login'),
    url(r'^signup/$', 'courses.views.signup', name='signup'),
    # url(r'^email/$', views.email, name="email"),
    
    url(r'^accounts/', include('allauth.urls')),
    url(r'^userdashboard/$', 'courses.views.userdashboard', name='userdashboard'),
    url(r'^mycourses/$', 'courses.views.mycourses', name='mycourses'),
    url(r'^users/$', 'courses.views.users', name='users'),
    url(r'^course/new/$', 'courses.views.new_course', name='new_course'),
    url(r'^course/(?P<pk>[0-9]+)/edit/$', 'courses.views.course_edit', name='course_edit'),
    
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
] 

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)