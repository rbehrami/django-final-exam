from django.shortcuts import render
from django_tables2 import RequestConfig
from courses.models import Course
from courses.tables import CourseTable



#from .forms import SignUpForm
def home(request):
	# return render(request, "home.html", {})
	table = CourseTable(Course.objects.all())
	RequestConfig(request).configure(table)
	return render(request, 'home.html', {'table': table})

def cslist(request):
	table = CourseTable(Course.objects.all())
	RequestConfig(request).configure(table)
	return render(request, 'cslist/cslist.html', {'table': table})
	
# def cslist(request):
#  	return render(request, "cslist/cslist.html", {"table": Course.objects.all()})


def about(request):
	return render(request, "about.html", {})

def contact_us(request):
	return render(request, "contact_us.html", {})

def login(request):
	return render(request, "login.html", {})

def signup(reqeust):
	return render(request, "signup.html", {})


